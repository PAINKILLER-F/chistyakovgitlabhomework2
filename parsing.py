"""Модуль для получения web-страницы в удобноый объект для парсинга"""
import requests
from bs4 import BeautifulSoup
import pandas as pd


def parse_url(request) :  # получение ссылок с сайта sports.ru
    """Получаем список ссылок на все команды АПЛ"""
    bs_object = BeautifulSoup(request.text, 'lxml')
    teams_links = []
    for i in bs_object.find_all('a', attrs={'class': "name"}):
        teams_links.append(i.get('href'))
    print("Ссылки на команды АПЛ с сайта sports.ru:")
    print(pd.Series(teams_links))
    return teams_links


def parse_teams(links_p):
    """Из списка ссылок на комады получаем датафрейм с информацией по всем игрокам"""
    squad_without_goalkeepers = []
    players_links1 = []
    positions = []
    countries = []
    for link in links_p:
        team_request = requests.get(link+"team/",timeout=5)
        bs1 = BeautifulSoup(team_request.text, 'lxml')
        for i in bs1.find_all('tr'):
            if i.find('td', attrs={'title': "вратарь"}) is None:
                if i.find('td', title=True) is not None:
                    positions.append(i.find('td', title=True).get('title'))
                    i_2 = i.find('td', attrs={'class': "name-td alLeft bordR"})
                    if i_2 is not None:
                        i_3 = i_2.find('a', href=True)
                        squad_without_goalkeepers.append(i_3.text)
                        players_links1.append(i_3.get('href'))
                        i_4 = i_2.find('i')
                        i_5 = i_4.get('title')
                        i_5 = i_5.replace("'", "")
                        countries.append(i_5)
    dataframe = pd.DataFrame()
    dataframe['Игрок'] = squad_without_goalkeepers
    dataframe['Ссылка'] = players_links1
    dataframe['Позиция'] = positions
    dataframe['Страна'] = countries
    print(dataframe)
    return dataframe


URL = 'https://www.sports.ru/epl/table/'

req = requests.get(URL,timeout=5)

print(req.status_code)

if req.status_code == 200:
    print("Соединение с сайтом установлено")
    links = parse_url(req)
    dataFrame = parse_teams(links)
    dataFrame.to_csv('C:/Users/4real/VSCode Projects/ChistyakovGitLabHomework/docks/1.csv')
else:
    print("Соединение с сайтом не установлено")
